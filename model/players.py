import datetime
from db import db
from sqlalchemy import func

class PlayerModel(db.Model):

    __tablename__ = 'T_PLAYERS'

    row_id = db.Column(db.Integer, primary_key = True)
    created = db.Column(db.DateTime)
    created_by = db.Column(db.Integer)
    updated = db.Column(db.DateTime)
    updated_by = db.Column(db.DateTime)
    player_id = db.Column(db.Integer)
    firstname = db.Column(db.String(100))
    lastname = db.Column(db.String(100))
    email = db.Column(db.String(100))
    mobile = db.Column(db.String(15))

    def __init__(self, created, created_by, updated, updated_by, player_id, firstname, lastname, email, mobile):
     #   self.id = row_id
        self.created = created
        self.created_by = created_by
        self.updated = updated
        self.updated_by = updated_by
        self.player_id = player_id
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.mobile = mobile

    def json(self):
        return {'created':self.created.isoformat(sep=' ', timespec = 'auto'), 'created_by': self.created_by, 'updated': self.updated.isoformat(sep=' ', timespec = 'auto'), 'updated_by': self.updated_by ,'player_id': self.player_id, 'firstname': self.firstname, 'lastname': self.lastname, 'email': self.email, 'mobile': self.mobile}

    def myconverter(self,o):
        self.o = o
        if isinstance(o, (datetime.datetime),):
            return o.__str__()

    @classmethod
    def find_player_by_lastname(cls,lastname):
        return cls.query.filter_by(lastname = lastname).first() #SELECT * FROM T_PLAYERS WHERE LASTNAME = LASTNAME
    
    
    def max_player_id():
        player_id = db.session.query(db.func.max(PlayerModel.player_id)).scalar()
        player_id = player_id + 1
        return player_id

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

