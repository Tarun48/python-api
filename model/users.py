from db import db
import pyodbc

class UserModel(db.Model):

    __tablename__ = 'T_USERS'

    row_id = db.Column(db.Integer, primary_key = True)
    created = db.Column(db.DateTime)
    updated = db.Column(db.DateTime)
    username = db.Column(db.String(100))
    password = db.Column(db.String(100))


    def __init__(self, _id, user_name, password, created, updated):
        self.id = _id
        self.username = user_name
        self.password = password
        self.created = created
        self.updated = updated

    def json(self):
        return {'id': self.id, 'username': self.user_name, 'password': self.password, 'created': self.created, 'updated': self.updated}   

    @classmethod 
    def find_by_username(cls, user_name):
        return cls.query.filter_by(username = user_name).first()
 
    @classmethod 
    def find_by_id(cls, _id):
        return cls.query.filter_by(id = _id).first()
 