from werkzeug.security import safe_str_cmp
from model.users import UserModel
from model.players import PlayerModel


def authenticate(username, password):
    user = UserModel.find_by_username(username)
    if user and safe_str_cmp(user.password, password):
            return user
    else:
            return {"message": "User not found in the system"}
        
def identity(payload):  
    _id = payload['identity']
    if _id:
            return UserModel.find_by_id(_id)
    else:
            return None    
 
 
