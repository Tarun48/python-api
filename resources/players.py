import pyodbc
import datetime
from flask_restful import Resource, reqparse,Api
from model.players import PlayerModel


class PlayerResource(Resource):

    def get(self, lastname):
        player = PlayerModel.find_player_by_lastname(lastname)
        if player:
            return player.json()
        return {'message': 'Player not found'},404
   
class NewPlayerResource(Resource):
    
    parser = reqparse.RequestParser()

    parser.add_argument(
                            'firstname',
                             type = str,
                             required = True,
                             help = "This field cannot be left blank"
                       )

    parser.add_argument(
                            'lastname',
                             type = str,
                             required = True,
                             help = "This field cannot be left blank"
                       )
    parser.add_argument(
                            'email',
                             type = str,
                             required = True,
                             help = "This field cannot be left blank"
                       )                   

    parser.add_argument(
                            'mobile',
                             type = str,
                             required = True,
                             help = "This field cannot be left blank"
                       )


    def post(self):     
        curr_date = datetime.datetime.now()
        data = NewPlayerResource.parser.parse_args() 
        player = PlayerModel(curr_date, 1, curr_date, 1, PlayerModel.max_player_id(), data['firstname'], data['lastname'], data['email'],data['mobile'])
        player.save_to_db()
        return player.json(),201
    
class UpdatePlayerResource(Resource):

    def put(self,lastname):      
        data = NewPlayerResource.parser.parse_args()
        curr_date = datetime.datetime.now()
        player = PlayerModel.find_player_by_lastname(lastname)
        if player is None:
            player = PlayerModel(curr_date, 1, curr_date, 1, PlayerModel.max_player_id(), data['firstname'], data['lastname'], data['email'],data['mobile'])
        else:
            player.firstname = data['firstname']
            player.lastname = data['lastname']
            player.email = data['email']
            player.mobile = data['mobile']
        player.save_to_db()
        return player.json()
    

    def delete(self, lastname):
        player = PlayerModel.find_player_by_lastname(lastname)
        if player:
            player.delete_from_db()
            return {'message': 'Player deleted'}
        else:
            return {'message': 'Player not found'}



 
