import pyodbc
import datetime
from flask_restful import Resource, reqparse
from model.users import UserModel

class UserRegister(Resource):

    parser = reqparse.RequestParser()

    parser.add_argument(
                            'username',
                             type = str,
                             required = True,
                             help = "This field cannot be left blank"
                       )

    parser.add_argument(
                            'password',
                             type = str,
                             required = True,
                             help = "This field cannot be left blank"
                       )                                            

    def post(self):
        
        data = UserRegister.parser.parse_args()
        curr_date = datetime.datetime.now()
        server = 'pmcc.cyps0ddzgfjp.ap-southeast-2.rds.amazonaws.com,1433'
        database = 'PMCC'
        username = 'admin'
        password = 'chrispmcc'
        connection = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+password)
        cursor = connection.cursor()
        query = "insert into T_USERS values (?, ?, ?, ?)"
        cursor.execute(query,data['username'], data['password'], curr_date, curr_date) # ,--> denotes tuples

        connection.commit()
        connection.close()

        return {"User": "{} succesfully created".format(data['username'])},201
    