from flask import Flask
from flask_jwt import JWT, jwt_required
from flask import Request, request
from flask_restful import Resource, Api, reqparse
from security import authenticate, identity
from resources.users import UserRegister
from resources.players import PlayerResource, NewPlayerResource, UpdatePlayerResource
from db import db
import urllib

params = urllib.parse.quote_plus("DRIVER={SQL Server};SERVER=pmcc.cyps0ddzgfjp.ap-southeast-2.rds.amazonaws.com,1433;DATABASE=PMCC;UID=admin;PWD=chrispmcc")
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "mssql+pyodbc:///?odbc_connect=%s" % params
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = '3d9911f6a21eaf80406a86d89b10e9afe4ab531482a79db6'
api = Api(app)

jwt = JWT(app, authenticate, identity) #/auth

api.add_resource(PlayerResource, '/player/<string:lastname>')
api.add_resource(NewPlayerResource, '/player/create')
api.add_resource(UpdatePlayerResource, '/player/<string:lastname>')
api.add_resource(UserRegister, '/register')

if __name__== '__main__':
    db.init_app(app)
    app.run(port = 5000, debug=True)